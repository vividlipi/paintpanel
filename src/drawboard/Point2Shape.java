package drawboard;

import java.awt.*;
import java.io.Serializable;

interface DrawMethod {
	public void draw(Graphics g);

	public void clear();
}

public abstract class Point2Shape implements DrawMethod, Serializable {
	private static final long serialVersionUID = 2L;
	int startx = 0, starty = 0, endx = 0, endy = 0;
	boolean filled = false;//是否填充颜色

	Graphics2D g2d;
	Color ocolor = null;//填充色
	Color icolor = null;//画笔色
	Color bcolor = null;//Panel背景色，clear方法使用bcolor覆盖内容

	public void setStartPoint(int x, int y) {
		startx = x;
		starty = y;
	}

	void setEndPoint(int x, int y) {
		endx = x;
		endy = y;
	}

	void setoColor(Color c) {
		this.ocolor = c;
	}

	void setiColor(Color c) {
		this.icolor = c;
	}
	
	void setbColor(Color c) {
		this.bcolor = c;
	}

	void setFill(boolean b) {
		filled = b;
	}
}

class rectangle extends Point2Shape implements DrawMethod {
	private static final long serialVersionUID = 1L;

	public void draw(Graphics g) {
		g2d = (Graphics2D) g;
		g2d.setColor(ocolor);
		g2d.drawRect(startx, starty, endx - startx, endy - starty);
		if (filled) {
			g2d.setColor(icolor);
			g2d.fillRect(startx + 1, starty + 1, endx - 1 - startx, endy - 1 - starty);
		}
	}

	public void clear() {
		g2d.setColor(bcolor);
		g2d.clearRect(startx, starty, endx - startx + 1, endy - starty + 1);
	}
}

class line extends Point2Shape implements DrawMethod {
	private static final long serialVersionUID = 1L;

	public void draw(Graphics g) {
		g2d = (Graphics2D) g;
		g2d.setColor(ocolor);
		g2d.drawLine(startx, starty, endx, endy);
	}

	public void clear() {
		g2d.setColor(bcolor);
		g2d.drawRect(startx, starty, endx, endy);
	}
}
class point extends Point2Shape implements DrawMethod{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	
}