package drawboard;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class UserInterface {
	JFrame mainFrame;
	JPanel topPanel;
	JPanel bottomPanel;
	JButton saveFile;
	JButton openFile;
	myButton fill;
	JLabel fColor;
	JLabel bColor;
	JColorChooser cChooser;
	buttonArray shapebutton = new buttonArray();
	forSaved forsaved;
	boolean saveFlag = true;

	UserInterface() {
		mainFrame = new JFrame("DrawBoard");
		topPanel = new JPanel();//按钮放在topPanel
		saveFile = new JButton("Save");
		openFile = new JButton("Open");
		fill = new myButton("fill");
		fColor = new JLabel();//画笔色
		bColor = new JLabel();//填充色
		cChooser = new JColorChooser();
		bottomPanel = new board();//画板
		forsaved = new forSaved();//存储图形对象

		fColor.setOpaque(true);
		fColor.setBackground(new Color(255, 255, 255));
		fColor.setPreferredSize(new Dimension(20, 20));
		fColor.setText("F");
		
		bColor.setOpaque(true);
		bColor.setBackground(new Color(255, 255, 255));
		bColor.setPreferredSize(new Dimension(20, 20));
		bColor.setText("B");

		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.setSize(800, 600);
		mainFrame.setResizable(false);
		mainFrame.add(topPanel, "North");
		mainFrame.add(bottomPanel, "Center");

		topPanel.add(openFile);
		topPanel.add(saveFile);
		
		for (int t = 0; t < shapebutton.total; t++) {//添加shapebutton中所有的button
			topPanel.add(shapebutton.myshape[t]);
		}
		
		topPanel.add(fill);
		topPanel.add(fColor);
		topPanel.add(bColor);

		bottomPanel.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				forsaved.create(shapebutton.onButton);
				forsaved.setStartPoint(arg0.getX(), arg0.getY());
				forsaved.setoColor(fColor.getBackground());
				forsaved.setiColor(bColor.getBackground());
				if (fill.onClick) {
					forsaved.setFill(true);
				}
			}

			public void mouseDragged(MouseEvent arg0) {
				forsaved.setEndPoint(arg0.getX(), arg0.getY());
				forsaved.draw(bottomPanel.getGraphics());
			}

			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				forsaved.setEndPoint(arg0.getX(), arg0.getY());
				forsaved.draw(bottomPanel.getGraphics());
				saveFlag = false;
			}
		});
		saveFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println(forsaved);	
				if (saveFlag) {
				} else {
					try {
						JFileChooser fileout = new JFileChooser();
						fileout.showSaveDialog(mainFrame);
						File file = fileout.getSelectedFile();
						ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file.getAbsolutePath()));
						out.writeObject(forsaved);
						out.flush();
						out.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		openFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				forSaved forread = null;
				if (saveFlag) {
					try {
						JFileChooser filein = new JFileChooser();
						filein.showOpenDialog(mainFrame);
						File file = filein.getSelectedFile();
						ObjectInputStream in = new ObjectInputStream(new FileInputStream(file.getAbsolutePath()));
						forread = (forSaved) in.readObject();
						forread.paintAll(bottomPanel.getGraphics());
						System.out.println("read object success!");
						in.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				} else {
					Object[] options = { "save", "not save", "cancel" };
					int n = JOptionPane.showOptionDialog(mainFrame,
							"Would you like to leave without save?", "A Silly Question",
							JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
					System.out.print(n);
					switch (n) {
					case 0: {
						saveFile.doClick();
						break;
					}
					case 1: {
						saveFlag = true;
						openFile.doClick();
						break;
					}
					case 2:
					case -1:
					default: {
					}
					}
				}
			}
		});
		class ColorChooseDialog extends MouseAdapter{
			public void mouseClicked(MouseEvent arg0) {
				Color bcolor = null;
				bcolor = JColorChooser.showDialog(mainFrame, "ColorChooser", bcolor);
				JLabel aLabel = (JLabel)( arg0.getSource());
				aLabel.setBackground(bcolor);
			}
		}
		fColor.addMouseListener(new ColorChooseDialog());
		bColor.addMouseListener(new ColorChooseDialog());
		
		
		
		mainFrame.setVisible(true);
	}

	public static void main(String[] args) {
		new UserInterface();
	}
}

class board extends JPanel {
	private static final long serialVersionUID = 1L;

	public void paint(Graphics g) {}

	public void repaint(Graphics g, forSaved s) {
		s.paintAll(g);
	}
}

class myButton extends JButton {
	private static final long serialVersionUID = 1L;
	boolean onClick = false;

	myButton(String arg0) {
		super(arg0);
		setstate();
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onClick = !onClick;
				setstate();
			}
		});
	}

	void setstate() {
		if (onClick) {
			setForeground(Color.PINK);
		} else {
			setForeground(Color.BLACK);
		}
	}
}

class myShapeButton extends myButton {
	private static final long serialVersionUID = 1L;
	int mynum = 0;

	myShapeButton(String arg0) {
		super(arg0);
		setstate();
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onClick = !onClick;
				setstate();
			}
		});
	}

}

class buttonArray {
	int total = 3;
	public myShapeButton[] myshape = new myShapeButton[100];
	int onButton = 0;

	buttonArray() {
		myshape[0] = new myShapeButton("squ");
		myshape[0].onClick = true;
		myshape[0].mynum = 0;
		myshape[0].setstate();

		myshape[1] = new myShapeButton("line");
		myshape[1].mynum = 1;
		
		myshape[2] = new myShapeButton("point");
		myshape[2].mynum = 2;

		class myshapebuttomlistener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				myShapeButton top;
				top = (myShapeButton) arg0.getSource();
				if (onButton != top.mynum) {
					onButton = top.mynum;
					top.onClick = !top.onClick;
					top.setstate();
					checkstate();
				}
			}
		}
		myshape[0].addActionListener(new myshapebuttomlistener());
		myshape[1].addActionListener(new myshapebuttomlistener());
		myshape[2].addActionListener(new myshapebuttomlistener());

	}

	void checkstate() {
		for (int m = 0; m < total; m++) {
			if (onButton != m) {
				myshape[m].onClick = false;
				myshape[m].setstate();
			}
		}
	}
}

class forSaved implements Serializable {
	private static final long serialVersionUID = 1L;
	Point2Shape[] some = new Point2Shape[100];
	Point2Shape next = null;
	int nowP = 0;

	forSaved() {
	}

	void create(int num) {
		switch (num) {
		case 0:
			next = new rectangle();
			break;
		case 1:
			next = new line();
			break;
		default:
			break;
		}
		some[nowP] = next;
		nowP++;
	}

	void setStartPoint(int x, int y) {
		some[nowP - 1].setStartPoint(x, y);
	}

	void setEndPoint(int x, int y) {
		some[nowP - 1].setEndPoint(x, y);
	}

	void setoColor(Color c) {
		some[nowP - 1].setoColor(c);
	}

	void setiColor(Color c) {
		some[nowP - 1].setiColor(c);
	}

	void draw(Graphics g) {
		some[nowP - 1].draw(g);
	}

	void clear(Graphics g) {
		some[nowP - 1].clear();
	}

	void setFill(boolean n) {
		some[nowP - 1].setFill(n);
	}

	void paintAll(Graphics g) {
		for (int i = 0; i < nowP; i++) {
			some[i].draw(g);
		}
	}
}